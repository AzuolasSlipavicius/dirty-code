<?php
declare(strict_types=1);

namespace App\Controller;


use App\Model\BillingHistory;

class BillingHistoryController
{
    public function __construct(private int $id)
    {
    }

    public function getBillingStatistics() {
        return BillingHistory::where('user_id', $this->id)
            ->groupBy('created_at')
            ->get();
    }
}