<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Payments;

class PaymentsController
{
    public function __construct(private int $id)
    {
    }

    public function addBalance($sum) {
        return $this->getBalance()->addSum($sum);
    }

    public function getBalance() {
        return $this->getBalance()->sum;
    }

    public function getPaymentsStatistics() {
        return Payments::where('user_id', $this->id)
            ->groupBy('created_at')
            ->get();
    }

}