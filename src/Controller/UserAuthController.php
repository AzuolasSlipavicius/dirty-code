<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\User;

class UserAuthController
{
    public string $email;

    public function login($email, $password): bool {
        $user = $this
            ->where('email', $email)
            ->get()
            ->first();

        return password_verify($password, $user->password);
    }

    public function register($email, $password):void {
        // some validation
        User::create([
            'email' => $email,
            'password' => password_hash($password, 'empty'),
        ]);
    }

    public function restorePassword($email): void {
        // validation
        $token = $this->createPasswordToken($email);
        User::update([
            'reset_token' => $token,
        ]);

        $this->email->send(
            $email, 'Password restore', 'views/auth/restorePassword', ['token' => $token]
        );
    }

    private function createPasswordToken($email): string
    {
        return md5($email);
    }
}