<?php

declare(strict_types=1);

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * This class is example of dirty code
 */
class User extends Model
{
    public function getFullName(): string {
        return $this->second_name . ' ' . $this->first_name;
    }
}