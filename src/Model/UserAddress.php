<?php
declare(strict_types=1);

namespace App\Model;

class UserAddress extends User
{
    public function getAddressString():string  {
        $address  = [
            $this->city,
            $this->street,
            $this->house,
            $this->apartment,
        ];

        return implode(" ", $address);
    }

}